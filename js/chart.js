function makeRequest (method, url, done) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, false);
    xhr.onload = function () {
      done(null, xhr.response);
    };
    xhr.onerror = function () {
      done(xhr.response);
    };
    xhr.send();
  }
  
makeRequest('GET', 'http://192.168.1.8/get_daily_humidity', function (err, data) {
    if (err) { throw err; }
    window.results = JSON.parse(data);
});



var datevalues = [];
window.results['dates'].forEach(element => datevalues.push(new Date(element)));
console.log(window.results['dates']);


var ctx = document.getElementById('chartCanvas').getContext('2d');
Chart.defaults.global.defaultFontColor = 'white';
var chart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: window.results['dates'],
        datasets: [{
            label: 'Umidità',
            backgroundColor: 'rgb(64, 196, 255)',
            borderColor: 'rgb(238, 255, 65)',
            data: window.results['avg_humidity']
        }]
    },
    options: {}
});

window.setInterval(function(){
        makeRequest('GET', 'http://192.168.1.8/get_humidity', function (err, data) {
        if (err) { throw err; }
        window.humidity = JSON.parse(data)["Humidity"];
        console.log(window.humidity);
    });
    document.querySelector("#humidity-level").innerText=window.humidity + " %";
  }, 500);

  window.setInterval(function(){
    makeRequest('GET', 'http://192.168.1.8/get_pumpstate', function (err, data) {
    if (err) { throw err; }
    window.status = JSON.parse(data)["Status"];
    console.log(window.status);
});
document.querySelector("#status").innerText=window.status;
}, 150);

window.setInterval(function(){
  makeRequest('GET', 'http://192.168.1.8/get_workingstate', function (err, data) {
  if (err) { throw err; }
  window.ws = JSON.parse(data)["WS"];
  console.log(window.ws);
});
document.querySelector("#working-mode").innerText=window.ws;
}, 150);